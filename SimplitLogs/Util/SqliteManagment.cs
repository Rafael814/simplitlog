﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;

namespace SimplitLogs.Util
{
    class SqliteManagment
    {
       public static List<string> GetData (string path, int col=0, string sSqlCustom = "SELECT * FROM TTraza_DTO")
       {
            return ReadData(CreateConnection(path),col,sSqlCustom);
       }

        static SQLiteConnection CreateConnection( string path)
        {

            SQLiteConnection sqlite_conn;
            // Create a new database connection:
            sqlite_conn = new SQLiteConnection("Data Source =  "+@path+"; Version = 3; New = True; Compress = True; ");
            // Open the connection:
            try
            {
                sqlite_conn.Open();
            }
            catch (Exception ex)
            {

            }
            return sqlite_conn;
        }

        static List<string>  ReadData (SQLiteConnection conn,int col, string sSql)
        {
            List<string> result = new List<string>();
            SQLiteDataReader sqlite_datareader;
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = conn.CreateCommand();
            sqlite_cmd.CommandText = sSql;

            sqlite_datareader = sqlite_cmd.ExecuteReader();
            while (sqlite_datareader.Read())
            {
                string myreader = sqlite_datareader.GetString(col);
                result.Add(myreader);
            }
            conn.Close();
            return result;
        }
    }
}
