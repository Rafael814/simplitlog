﻿namespace SimplitLogs
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.menuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadLogFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vieErrorsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportErrorsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.m1Module = new System.Windows.Forms.ComboBox();
            this.m1OutPut = new System.Windows.Forms.RichTextBox();
            this.m1Copy = new System.Windows.Forms.Button();
            this.directoryEntry1 = new System.DirectoryServices.DirectoryEntry();
            this.m1Path = new System.Windows.Forms.TextBox();
            this.directorySearcher1 = new System.DirectoryServices.DirectorySearcher();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.m1Load = new System.Windows.Forms.Button();
            this.m1SelectModule = new System.Windows.Forms.Label();
            this.m1DateI = new System.Windows.Forms.TextBox();
            this.m1DateF = new System.Windows.Forms.TextBox();
            this.m1lbldatei = new System.Windows.Forms.Label();
            this.m1lbldatef = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.White;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(799, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // menuToolStripMenuItem
            // 
            this.menuToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadLogFileToolStripMenuItem,
            this.vieErrorsToolStripMenuItem,
            this.reportErrorsToolStripMenuItem,
            this.settingsToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.menuToolStripMenuItem.Name = "menuToolStripMenuItem";
            this.menuToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.menuToolStripMenuItem.Text = "Menu";
            this.menuToolStripMenuItem.Click += new System.EventHandler(this.menuToolStripMenuItem_Click);
            // 
            // loadLogFileToolStripMenuItem
            // 
            this.loadLogFileToolStripMenuItem.Name = "loadLogFileToolStripMenuItem";
            this.loadLogFileToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.loadLogFileToolStripMenuItem.Text = "Load LogFile";
            this.loadLogFileToolStripMenuItem.Click += new System.EventHandler(this.loadLogFileToolStripMenuItem_Click);
            // 
            // vieErrorsToolStripMenuItem
            // 
            this.vieErrorsToolStripMenuItem.Name = "vieErrorsToolStripMenuItem";
            this.vieErrorsToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.vieErrorsToolStripMenuItem.Text = "View Errors";
            this.vieErrorsToolStripMenuItem.Click += new System.EventHandler(this.vieErrorsToolStripMenuItem_Click);
            // 
            // reportErrorsToolStripMenuItem
            // 
            this.reportErrorsToolStripMenuItem.Name = "reportErrorsToolStripMenuItem";
            this.reportErrorsToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.reportErrorsToolStripMenuItem.Text = "Report Errors";
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.settingsToolStripMenuItem.Text = "Settings";
            this.settingsToolStripMenuItem.Click += new System.EventHandler(this.settingsToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // m1Module
            // 
            this.m1Module.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.m1Module.FormattingEnabled = true;
            this.m1Module.Location = new System.Drawing.Point(122, 86);
            this.m1Module.Name = "m1Module";
            this.m1Module.Size = new System.Drawing.Size(255, 21);
            this.m1Module.TabIndex = 1;
            this.m1Module.Visible = false;
            this.m1Module.SelectedIndexChanged += new System.EventHandler(this.m1Module_SelectedIndexChanged);
            // 
            // m1OutPut
            // 
            this.m1OutPut.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m1OutPut.Location = new System.Drawing.Point(122, 113);
            this.m1OutPut.Name = "m1OutPut";
            this.m1OutPut.Size = new System.Drawing.Size(665, 289);
            this.m1OutPut.TabIndex = 2;
            this.m1OutPut.Text = "";
            this.m1OutPut.Visible = false;
            this.m1OutPut.TextChanged += new System.EventHandler(this.richTextBox1_TextChanged);
            // 
            // m1Copy
            // 
            this.m1Copy.Location = new System.Drawing.Point(712, 408);
            this.m1Copy.Name = "m1Copy";
            this.m1Copy.Size = new System.Drawing.Size(75, 23);
            this.m1Copy.TabIndex = 3;
            this.m1Copy.Text = "Copy";
            this.m1Copy.UseVisualStyleBackColor = true;
            this.m1Copy.Visible = false;
            this.m1Copy.Click += new System.EventHandler(this.m1Copy_Click);
            // 
            // m1Path
            // 
            this.m1Path.Location = new System.Drawing.Point(240, 4);
            this.m1Path.Name = "m1Path";
            this.m1Path.Size = new System.Drawing.Size(396, 20);
            this.m1Path.TabIndex = 4;
            this.m1Path.Visible = false;
            this.m1Path.TextChanged += new System.EventHandler(this.m1Path_TextChanged);
            // 
            // directorySearcher1
            // 
            this.directorySearcher1.ClientTimeout = System.TimeSpan.Parse("-00:00:01");
            this.directorySearcher1.ServerPageTimeLimit = System.TimeSpan.Parse("-00:00:01");
            this.directorySearcher1.ServerTimeLimit = System.TimeSpan.Parse("-00:00:01");
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // m1Load
            // 
            this.m1Load.Location = new System.Drawing.Point(369, 27);
            this.m1Load.Name = "m1Load";
            this.m1Load.Size = new System.Drawing.Size(129, 30);
            this.m1Load.TabIndex = 5;
            this.m1Load.Text = "Cargar";
            this.m1Load.UseVisualStyleBackColor = true;
            this.m1Load.Visible = false;
            this.m1Load.Click += new System.EventHandler(this.m1Load_Click);
            // 
            // m1SelectModule
            // 
            this.m1SelectModule.AutoSize = true;
            this.m1SelectModule.Location = new System.Drawing.Point(119, 70);
            this.m1SelectModule.Name = "m1SelectModule";
            this.m1SelectModule.Size = new System.Drawing.Size(101, 13);
            this.m1SelectModule.TabIndex = 6;
            this.m1SelectModule.Text = "Seleccionar Módulo";
            this.m1SelectModule.Visible = false;
            // 
            // m1DateI
            // 
            this.m1DateI.Location = new System.Drawing.Point(403, 87);
            this.m1DateI.Name = "m1DateI";
            this.m1DateI.ReadOnly = true;
            this.m1DateI.Size = new System.Drawing.Size(175, 20);
            this.m1DateI.TabIndex = 7;
            this.m1DateI.Visible = false;
            // 
            // m1DateF
            // 
            this.m1DateF.Location = new System.Drawing.Point(612, 86);
            this.m1DateF.Name = "m1DateF";
            this.m1DateF.ReadOnly = true;
            this.m1DateF.Size = new System.Drawing.Size(175, 20);
            this.m1DateF.TabIndex = 8;
            this.m1DateF.Visible = false;
            // 
            // m1lbldatei
            // 
            this.m1lbldatei.AutoSize = true;
            this.m1lbldatei.Location = new System.Drawing.Point(400, 70);
            this.m1lbldatei.Name = "m1lbldatei";
            this.m1lbldatei.Size = new System.Drawing.Size(65, 13);
            this.m1lbldatei.TabIndex = 9;
            this.m1lbldatei.Text = "Fecha Inicio";
            this.m1lbldatei.Visible = false;
            // 
            // m1lbldatef
            // 
            this.m1lbldatef.AutoSize = true;
            this.m1lbldatef.Location = new System.Drawing.Point(609, 70);
            this.m1lbldatef.Name = "m1lbldatef";
            this.m1lbldatef.Size = new System.Drawing.Size(51, 13);
            this.m1lbldatef.TabIndex = 10;
            this.m1lbldatef.Text = "Fecha fin";
            this.m1lbldatef.Visible = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(799, 453);
            this.Controls.Add(this.m1lbldatef);
            this.Controls.Add(this.m1lbldatei);
            this.Controls.Add(this.m1DateF);
            this.Controls.Add(this.m1DateI);
            this.Controls.Add(this.m1SelectModule);
            this.Controls.Add(this.m1Load);
            this.Controls.Add(this.m1Path);
            this.Controls.Add(this.m1Copy);
            this.Controls.Add(this.m1OutPut);
            this.Controls.Add(this.m1Module);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "SpLogs";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem menuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadLogFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vieErrorsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reportErrorsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ComboBox m1Module;
        private System.Windows.Forms.RichTextBox m1OutPut;
        private System.Windows.Forms.Button m1Copy;
        private System.DirectoryServices.DirectoryEntry directoryEntry1;
        private System.Windows.Forms.TextBox m1Path;
        private System.DirectoryServices.DirectorySearcher directorySearcher1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.Button m1Load;
        private System.Windows.Forms.Label m1SelectModule;
        private System.Windows.Forms.TextBox m1DateI;
        private System.Windows.Forms.TextBox m1DateF;
        private System.Windows.Forms.Label m1lbldatei;
        private System.Windows.Forms.Label m1lbldatef;
    }
}

