﻿using SimplitLogs.Util;
using SimplitLogs.View;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SimplitLogs
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void menuToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void loadLogFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            VisibleM1();
            OpenFileDialogEvent();
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void VisibleM1()
        {
            m1Module.Visible = true;
            m1OutPut.Visible = true;
            m1Copy.Visible = true;
            m1Path.Visible = true;
            m1Load.Visible = true;
            m1DateI.Visible = true;
            m1DateF.Visible = true;
            m1SelectModule.Visible = true;
            m1lbldatei.Visible = true;
            m1lbldatef.Visible = true;
            


        }

        private void OpenFileDialogEvent()
        {

            OpenFileDialog openFileDialog1 = new OpenFileDialog
            {
                InitialDirectory = @"C:\Users\Joselyn\ATA\Repos Home\CoreSp\CoreSimplitPos\CoreSimplitPos\bin\Debug\netcoreapp2.0\win7-x86\db\",
                Title = "Browse Text Files",

                CheckFileExists = true,
                CheckPathExists = true,

                DefaultExt = "sqlite",
                Filter = "sqlite files (*.sqlite)|*.sqlite",
                FilterIndex = 2,
                RestoreDirectory = true,

                ReadOnlyChecked = true,
                ShowReadOnly = true
            };

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                m1Path.Text = openFileDialog1.FileName;
            }

        }

        private void m1Path_TextChanged(object sender, EventArgs e)
        {

        }

        private void m1Load_Click(object sender, EventArgs e)
        {
         m1Module.DataSource = SqliteManagment.GetData(m1Path.Text,1);
        }

        private void m1Module_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (m1Module.Text.Length > 0)
            {
                string sSql = @"SELECT * 
                                FROM TRAZA_DTO 
                                WHERE MODULO = " + m1Module.Text;

                var oResult = SqliteManagment.GetData(m1Path.Text, 2);
                if (oResult.Count > 0)
                {
                    m1OutPut.Text = oResult[0].ToString();




                    m1DateI.Text = SqliteManagment.GetData(m1Path.Text, 3)[0].ToString();
                    m1DateF.Text = SqliteManagment.GetData(m1Path.Text, 4)[0].ToString();

                }
            }

        }

        private void m1Copy_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(m1OutPut.Text);
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void vieErrorsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            VisibleM1();
            m1Path.Text = Properties.Settings.Default.defPath;
        }

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SettingsSP sp = new SettingsSP();
            sp.Show();

        }
    }
}
