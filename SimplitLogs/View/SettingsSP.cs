﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SimplitLogs.View
{
    public partial class SettingsSP : Form
    {
        public SettingsSP()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (pathDef.Text.Length > 0)
            {
                Properties.Settings.Default.defPath = pathDef.Text;
                this.Dispose();
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {


                OpenFileDialog openFileDialog1 = new OpenFileDialog
                {
                    InitialDirectory = @"D:\",
                    Title = "Browse Text Files",

                    CheckFileExists = true,
                    CheckPathExists = true,

                    DefaultExt = "sqlite",
                    Filter = "sqlite files (*.sqlite)|*.sqlite",
                    FilterIndex = 2,
                    RestoreDirectory = true,

                    ReadOnlyChecked = true,
                    ShowReadOnly = true
                };

                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    pathDef.Text = openFileDialog1.FileName;
                }
        }
    }
}
